import 'package:cost_of_care/bloc/compare_hospital_bloc/compare_hospital_screen/compare_hospital_screen_bloc.dart';
import 'package:cost_of_care/bloc/detail_hospital_bloc/detail_hospital_list_bloc.dart';
import 'package:cost_of_care/main.dart';
import 'package:cost_of_care/models/hospitals.dart';

import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';

import 'detail_information.dart';

// ignore: must_be_immutable
class BodyDetail extends StatefulWidget {
  BodyDetail(this.detailHospitalListBloc, this.hospital);
  DetailHospitalListBloc detailHospitalListBloc;
  Hospitals hospital;

  @override
  State<BodyDetail> createState() => _BodyDetailState();
}

class _BodyDetailState extends State<BodyDetail> {
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: widget.detailHospitalListBloc,
      listener: (BuildContext context, state) {
        if (state is ShowSnackBar) {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text(
              state.message,
              style: TextStyle(color: Colors.white),
            ),
            backgroundColor: Colors.red,
          ));
        }
      },
      child: BlocBuilder(
          bloc: widget.detailHospitalListBloc,
          builder: (BuildContext context, DetailHospitalListState state) {
            if (state is LoadingState) {
              return Center(child: CircularProgressIndicator());
            } else if (state is LoadedStateDetail) {
              widget.detailHospitalListBloc.add(UpdateHospitalToCompare(0));
              widget.detailHospitalListBloc.add(
                  FloatingCompareHospitalButtonPress(
                      BlocProvider.of<CompareHospitalScreenBloc>(context)));

              return Container(
                child: Column(
                  children: [
                    Text(
                      widget.hospital.name,
                      overflow: TextOverflow.visible,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontFamily: "Source",
                          fontSize: 22,
                          fontWeight: FontWeight.bold),
                    ),
                    DetailBody()
                  ],
                ),
              );
            } else if (state is ErrorState) {
              return Column(children: [
                Text(
                  widget.hospital.name,
                  overflow: TextOverflow.visible,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontFamily: "Source",
                      fontSize: 22,
                      fontWeight: FontWeight.bold),
                ),
                const SizedBox(
                  height: 20,
                ),
                Text(
                  "No records found !",
                  style: TextStyle(
                      fontFamily: "Source",
                      fontSize: 17,
                      color: Colors.grey[700]),
                )
              ]);
            }
            return CircularProgressIndicator();
          }),
    );
  }

 

  @override
  void initState() {
    super.initState();
    widget.detailHospitalListBloc.add(GetCompareData());
    widget.detailHospitalListBloc.add(SearchCompareData(
        widget.hospital.name.toLowerCase().replaceAll(" - ", "-"),
        box.get('state')));
  }
}
