part of 'detail_hospital_list_bloc.dart';

abstract class DetailHospitalListEvent extends Equatable {
  const DetailHospitalListEvent();
}

class GetCompareData extends DetailHospitalListEvent {
  @override
  List<Object> get props => [];
}

class SearchCompareData extends DetailHospitalListEvent {
  final String query;
  final String stateName;

  SearchCompareData(this.query, this.stateName);
  @override
  List<Object> get props => [query, stateName];
}

class UpdateHospitalToCompare extends DetailHospitalListEvent {
  final int index;
  // final bool filteredlist;
  UpdateHospitalToCompare(this.index);
  @override
  List<Object> get props => [index];
}

class FloatingCompareHospitalButtonPress extends DetailHospitalListEvent {
  final CompareHospitalScreenBloc compareHospitalScreenBloc;

  FloatingCompareHospitalButtonPress(this.compareHospitalScreenBloc);
  @override
  List<Object> get props => [compareHospitalScreenBloc];
}
